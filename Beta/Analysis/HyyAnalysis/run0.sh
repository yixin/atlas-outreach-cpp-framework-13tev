## small script to run the analysis
analysis="main_HyyAnalysis"
## for now, turn off Proof
parallel=0

##OPTION
option=$1

## execute and run ROOT
echo "starting ROOT"

##
root -l -b << EOF
.L $analysis.C
$analysis($parallel,$option)
EOF

##
echo "end of ROOT execution"
